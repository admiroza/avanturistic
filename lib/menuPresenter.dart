import 'package:avanturistic/data/rest.dart';
import 'package:avanturistic/models/user.dart';

abstract class NavDrawerContract {
  void onLogout();
}

class NavDrawerPresenter {
  NavDrawerContract _view;
  NavDrawerPresenter(this._view);

  logout() {
    _view.onLogout();
  }
}