import 'package:flutter/material.dart';
import '../models/post.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:transparent_image/transparent_image.dart';

class PostRow extends StatelessWidget {
  final Post _post;
  PostRow(this._post);

  @override
  Widget build(BuildContext context) => Container(
    margin: const EdgeInsets.only(left: 10.0, right: 10.0,top:10.0, bottom: 10.0),
     child: Column(

       children: <Widget>[
         Container(
           decoration: new BoxDecoration(
             color: Colors.black,
           ),
           height: 60.0,
           child: new Column(

             children: <Widget>[
               Row(
                 children: <Widget>[
                   Container(
                     margin: const EdgeInsets.only(left: 5.0, right: 10.0,top:5.0),
                     width: 50.0,
                     height: 50.0,
                     decoration: BoxDecoration(
                       shape: BoxShape.circle,
                       image: DecorationImage(
                         image: CachedNetworkImageProvider(this._post.avatar != null ? 'https://avanturistic.com/' + this._post.avatar : ''),
                       ),
                     ),
                   ),
                   Text(_post.username, style: TextStyle(color: Colors.white))
                 ],
               )


             ],
           ),
         ),

         FadeInImage.memoryNetwork(
           placeholder: kTransparentImage,
           image: 'https://avanturistic.com' + _post.images[0].thumb_path,
         ),
         if(this._post.badges.length > 0)
           SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child: Row(
                 children:List.generate(this._post.badges.length,(index){
                   return Column(
                     children: <Widget>[

                       Container(
                         margin: const EdgeInsets.only(left: 10.0, right: 10.0,top:10.0,bottom:10.0),
                         width: 30.0,
                         height: 30.0,
                         decoration: BoxDecoration(
                           shape: BoxShape.circle,
                           image: DecorationImage(
                             image: CachedNetworkImageProvider(this._post.badges.length > 0 != null ? 'https://avanturistic.com/img/badges/' + this._post.badges[index] + '.png' : ''),
                           ),
                         ),
                       ),
                     ],
                   );
                 })
             ),
           ),
         Row(
           children: <Widget>[
             Center(
               child: Row(
                 children: <Widget>[
                   Column(
                     children: <Widget>[
                       Icon(Icons.thumb_up),
                       Text(_post.likes.toString())
                     ],
                   ),
                   Column(
                     children: <Widget>[
                       Icon(Icons.flight_land),
                       Text(_post.visiteds.toString())
                     ],
                   ),
                   Column(
                     children: <Widget>[
                       Icon(Icons.comment),
                       Text(_post.commentsCount.toString())
                     ],
                   ),
                   Column(
                     children: <Widget>[
                       Icon(Icons.map),
                       Text(_post.distance.toStringAsFixed(2) + 'km')
                     ],
                   )
                 ],
               ),

             )
           ],
         )

       ],
     ),
  );

}