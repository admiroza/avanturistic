class User {
  String _email;
  String _password;
  String _name;
  User(this._email, this._password, this._name);
  String get email => _email;
  String get password => _password;
  String get name => _name;

  User.map(dynamic obj) {
    this._email = obj["email"];
    this._password = obj["password"];
    this._name = obj["name"];
  }
  User.fromMap(dynamic obj) {
    this._email = obj['email'];
    this._password = obj['password'];
    this._name = obj["name"];

  }


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["email"] = _email;
    map["password"] = _password;
    map["name"] = _name;

    return map;
  }


}