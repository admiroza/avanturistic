class Image {
  String path;
  String thumb_path;


  Image({this.path, this.thumb_path});

  Image.map(dynamic jsonMap) {
    print(jsonMap['thumb_path']);
    this.thumb_path = jsonMap['thumb_path'];
    this.path = jsonMap['path'];
  }

}