import 'package:avanturistic/models/image.dart';
import 'dart:convert';

class Post {
  int id;
  int userId;
  String title;
  String description;
  String options;
  List<Image> images;
  List<String> badges;

  String username;
  String address;
  String avatar;

  int isLiked = 0;
  int isVisited = 0;

  int likes = 0;
  int visiteds = 0;
  int commentsCount = 0;
  double distance;
  String lat;
  String lng;

  Post({this.id, this.userId, this.title, this.options, this.images, this.badges, this.avatar, this.username, this.description, this.likes, this.visiteds, this.commentsCount, this.isLiked
  , this.isVisited, this.lat, this.lng, this.address});

  Post.fromJSON(Map<String, dynamic> jsonMap) {
    this.id = jsonMap['id'];
    this.userId = jsonMap['user_id'];
    this.title = jsonMap['title'];
    this.badges = jsonMap['badges'];
    this.avatar = jsonMap['avatar'];
    this.username = jsonMap['username'];
    this.description = jsonMap['description'];
    this.likes = jsonMap['likes'];
    this.lat = jsonMap['lat'];
    this.lng = jsonMap['lng'];
    this.address = jsonMap['address'];
    this.distance = jsonMap['distance'];
    this.visiteds = jsonMap['visiteds'];
    this.commentsCount = jsonMap['comments_count'];
    this.isLiked = jsonMap['isLiked'];
    this.isVisited = jsonMap['isVisited'];
    this.images = [];

  }

  Post.map(dynamic obj) {
    this.id = obj["id"];
    this.userId = obj["userId"];
    this.title = obj["title"];
    this.description = obj["description"];
    this.username = obj["username"];
    this.avatar = obj["avatar"];
    this.likes = obj["likes"];
    this.visiteds = obj["visiteds"];
    this.commentsCount = obj["comments_count"];
    this.isVisited = obj["isVisited"];
    this.lat = obj["lat"];
    this.lng = obj["lng"];
    this.address = obj['address'];
    this.distance = obj["distance"];
    this.isLiked = obj["isLiked"];
    this.images = [];
    this.badges = [];

    var options = null;
    final _imageList =  json.decode(obj["image"]);
    if(obj["opts"] != null)
     options =  json.decode(obj["opts"]);

    if(options != null && options["badges"] != null ){
    print(options["badges"].toString());
    options["badges"].forEach((final String key, final value) {
      this.badges.add(key);
    });
    }


    for (var i = 0; i < (_imageList.length); i++) {
    this.images.add(new Image.map(_imageList[i]));
    }
  }

}