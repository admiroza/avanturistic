import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:avanturistic/models/post.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class PostDetailScreen extends StatelessWidget {
  // Declare a field that holds the Todo.
  final Post post;

  // In the constructor, require a Todo.
  PostDetailScreen({Key key, @required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(post.lat);
    var lat = double.parse(post.lat);
    var lng = double.parse(post.lng);
    FlutterMap map =  FlutterMap(
      options: MapOptions(
        center: LatLng(lat, lng),
        zoom: 10.0,
        maxZoom: 15.0,
        minZoom: 2.0,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate:
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            subdomains: ['a', 'b', 'c']),
        new MarkerLayerOptions(
          markers: [
            new Marker(
              width: 30.0,
              height: 30.0,
              point: new LatLng(lat, lng),
              builder: (ctx) =>
              new Container(
                child:  Image.asset(
                    'assets/img/logo.png'
                ),
              ),
            ),
          ],
        ),
      ],

    );

    // Use the Todo to create the UI.
    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),
      ),
      body: SingleChildScrollView(
        child:  Column(
          children: <Widget>[
            FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: 'https://avanturistic.com' + post.images[0].thumb_path,
            ),
            if(this.post.badges.length > 0)
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                    children:List.generate(this.post.badges.length,(index){
                      return Column(
                        children: <Widget>[

                          Container(
                            margin: const EdgeInsets.only(left: 10.0, right: 10.0,top:10.0,bottom:10.0),
                            width: 30.0,
                            height: 30.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: CachedNetworkImageProvider(this.post.badges.length > 0 != null ? 'https://avanturistic.com/img/badges/' + this.post.badges[index] + '.png' : ''),
                              ),
                            ),
                          ),
                        ],
                      );
                    })
                ),
              ),
            Row(
              children: <Widget>[
                Center(
                  child: Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Icon(Icons.thumb_up),
                          Text(post.likes.toString())
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Icon(Icons.flight_land),
                          Text(post.visiteds.toString())
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Icon(Icons.comment),
                          Text(post.commentsCount.toString())
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Icon(Icons.map),
                          Text(post.distance.toStringAsFixed(2) + 'km')
                        ],
                      )
                    ],
                  ),

                ),

              ],
            ),
            if(post.title != null)
              Container(
                height: 50,
                child:
                Text(post.title),
              ),
            if(post.description != null)
              Container(
                height: 50,
                child:
                Text(post.description),
              ),
            if(post.address != null)
              Container(
                height: 50,
                child:
                Text(post.address),
              ),

            Container(
              height: 300.00,
              child: map,
            )
          ],

        ),
      )

    );
  }
}