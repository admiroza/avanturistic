
import 'dart:ui';
import 'package:avanturistic/screens/post/detail.dart';
import 'package:flutter/material.dart';
import 'package:avanturistic/menu.dart';
import 'package:avanturistic/auth.dart';

import 'package:avanturistic/data/rest.dart';
import 'package:avanturistic/data/database.dart';
import 'package:avanturistic/widgets/post_row.dart';
import 'package:avanturistic/models/post.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';

class HomeScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    implements AuthStateListener {
  BuildContext _ctx;
  List<Post> postsList = <Post>[];
  final formKey = new GlobalKey<FormState>();
  String _searchQuery = '';

  @override
  void initState() {
    super.initState();
    listenForPosts();
  }
  void listenForPosts() async {
    RestDatasource api = new RestDatasource();

    api.getPosts('admir@omnitask.ba').then((List<Post> post) {
      print('set post list');
     setState(() {
       postsList = post;
     });
    }).catchError((Object error) => print(error));
  }
  void _geocode (text) async{
    List<Placemark> placemark = await Geolocator().placemarkFromAddress(text);
    print(placemark[0].position.latitude);
  }

  void _getLocation() async{
    Position position = await Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    print(position);
  }

  @override
  onAuthStateChanged(AuthState state) {
    print('auth state changed');
    if (state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
    if (state == AuthState.LOGGED_OUT)
      Navigator.of(_ctx).pushReplacementNamed("/login");
  }

  @override
  Widget build(BuildContext context) {

    _getLocation();
    var searchForm = new Form(
      key: formKey,
      child: new Column(
        children: <Widget>[
          TextFormField(
            onSaved: (val) => _searchQuery = val,
            onChanged:(text) => _geocode(text),
            decoration: new InputDecoration(labelText: "Search places or addresses"),
          ),
        ],
      ),
    );
    FlutterMap map =  FlutterMap(
      options: MapOptions(
        center: LatLng(51.5, -0.09),
        zoom: 2.0,
        maxZoom: 15.0,
        minZoom: 2.0,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate:
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            subdomains: ['a', 'b', 'c']),
        new MarkerLayerOptions(
          markers: [
            new Marker(
              width: 30.0,
              height: 30.0,
              point: new LatLng(51.5, -0.09),
              builder: (ctx) =>
              new Container(
                child:  Image.asset(
                    'assets/img/logo.png'
                ),
              ),
            ),
          ],
        ),
      ],

    );

    return new Scaffold(
      drawer: NavDrawer(),

      appBar: new AppBar(
        title:
        new Text("AVANTURISTIC", style: new TextStyle(
          color: Colors.white)
      ),

      ),
      body:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300.0,
              child: new Center(
                child: new ClipRect(
                  child:  new Column(
                    children: [
                      searchForm,
                      Flexible(
                        child: FlutterMap(
                          options: MapOptions(
                            center: LatLng(51.5, -0.09),
                            zoom: 2.0,
                            maxZoom: 15.0,
                            minZoom: 2.0,
                          ),
                          layers: [
                            TileLayerOptions(
                                urlTemplate:
                                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                                subdomains: ['a', 'b', 'c']),
                            new MarkerLayerOptions(
                              markers: [
                                new Marker(
                                  width: 30.0,
                                  height: 30.0,
                                  point: new LatLng(51.5, -0.09),
                                  builder: (ctx) =>
                                  new Container(
                                    child:  Image.asset(
                                        'assets/img/logo.png'
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],

                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            Column(
                children:List.generate(this.postsList.length,(index){
                  return
                    Container(
                      child: GestureDetector(
                        child: PostRow(postsList[index]),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PostDetailScreen(post: postsList[index]),
                            ),
                          );
                        },
                      ),
                    );
                })
            ),
          ],
        ),
      )




    );

  }

}