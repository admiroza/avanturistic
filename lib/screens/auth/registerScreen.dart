import 'dart:ui';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:avanturistic/auth.dart';
import 'package:avanturistic/data/database.dart';
import 'package:avanturistic/models/user.dart';
import 'package:avanturistic/screens/auth/registerScreenPresenter.dart';
import 'package:email_validator/email_validator.dart';


class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen>
    implements RegisterScreenContract, AuthStateListener {
  BuildContext _ctx;
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _password, _email, _name,_passwordConfirmation;

  RegisterScreenPresenter _presenter;

  RegisterScreenState() {
    _presenter = new RegisterScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }
  Future<bool> _onBackPressed() {
    Navigator.of(_ctx).pushReplacementNamed("/login");

  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print('submit');
      _presenter.doRegister(_email, _password, _name);
    }
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
    if (state == AuthState.LOGGED_OUT)
      Navigator.of(_ctx).pushReplacementNamed("/login");
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    final TextEditingController _passInput = TextEditingController();
    final TextEditingController _confirmPassInput = TextEditingController();

    var registerBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Sign Up"),
      color: Colors.white,
    );
    var registerForm =
    new Container(
        margin: const EdgeInsets.only(left: 20.0, right: 20.0,top:60.0),
        child: new Column(
          children:
          <Widget>[
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    'assets/img/logo.png', width: 60.0, height: 60.0,
                  ),
                ],
              ),
            ),
            new Padding(
              padding: const EdgeInsets.all(8.0),
              child:  new Text(
                "Create Account",
                textScaleFactor: 1,
                style: new TextStyle(
                    color: Colors.white),
              ),
            ),

            new Form(
              key: formKey,
              child: new Column(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(

                      onSaved: (val) => _email = val,
                      validator: (val) {
                        if(!(EmailValidator.validate(val)))
                          return 'Enter valid email address.';

                         return null;
                      },
                      decoration: new InputDecoration(labelText: "Email"),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      onSaved: (val) => _name = val,
                      validator: (val) {
                        return val.length < 3
                            ? "Name must have at least 3 chars"
                            : null;
                      },
                      decoration: new InputDecoration(labelText: "Name"),
                    ),
                  ),

                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      controller: _passInput,
                      obscureText: true,
                      validator: (val) {
                        if(val.isEmpty)
                          return 'Password is required.';
                        return null;
                      },
                      onSaved: (val) => _password = val,
                      decoration: new InputDecoration(labelText: "Password"),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      obscureText: true,
                      controller: _confirmPassInput,
                      validator: (val) {
                        if(val.isEmpty)
                          return 'Password confirmation is required.';
                        if(val != _passInput.text)
                          return 'Password doesn\'t match';

                        return null;
                      },
                      onSaved: (val) => _passwordConfirmation = val,
                      decoration: new InputDecoration(labelText: "Re-type Password"),
                    ),
                  ),
                ],
              ),
            ),

            _isLoading ?  new CircularProgressIndicator() : registerBtn,

          ],
          crossAxisAlignment: CrossAxisAlignment.center,
        )
    );

    return new WillPopScope(
      onWillPop: _onBackPressed,
      child: new Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/img/cover.jpg"),
              fit: BoxFit.cover),
        ),
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                child: registerForm,

                decoration: new BoxDecoration(
                    color: Colors.white.withOpacity(0.3)),
              ),
            ),
          ),
        ),
      ),
    ),
    );

  }

  @override
  void onRegisterError(String errorTxt) {
    print('on reg. error');
    _showSnackBar(errorTxt);
  //  print(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onRegisterSuccess(User user) async {
    //_showSnackBar(user.toString());
    setState(() => _isLoading = false);
    var db = new DatabaseHelper();
    await db.saveUser(user);

    Navigator.of(_ctx).pushReplacementNamed("/home");
  }
}