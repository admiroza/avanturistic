import 'dart:ui';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:avanturistic/auth.dart';
import 'package:avanturistic/data/database.dart';
import 'package:avanturistic/models/user.dart';
import 'package:avanturistic/screens/auth/loginScreenPresenter.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:avanturistic/data/rest.dart';
import 'package:email_validator/email_validator.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen>
    implements LoginScreenContract, AuthStateListener {
  BuildContext _ctx;
  static final FacebookLogin facebookSignIn = new FacebookLogin();

  String _message = 'Log in/out by pressing the buttons below.';
  Future<bool> _onBackPressed() {
    return null;

  }
  Future<Null> _login() async {
    final FacebookLoginResult result =
    await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        _showMessage('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,email&access_token=${accessToken.token}');
        final JsonDecoder _decoder = new JsonDecoder();
        var res =  _decoder.convert(graphResponse.body);

        RestDatasource api = new RestDatasource();

        api.register(res['email'], accessToken.token, res['name']).then((User user) {

        }).catchError((Object error) => print(error));
        var user = new User(res['email'], 'pass', res['name']);
        var db = new DatabaseHelper();
        db.saveUser(user);
        Navigator.of(_ctx).pushReplacementNamed("/home");
        break;
      case FacebookLoginStatus.cancelledByUser:
        _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        _showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
    _showMessage('Logged out.');
  }

  void _showMessage(String message) {
    setState(() {
      _message = message;
    });
  }
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _password, _email;

  LoginScreenPresenter _presenter;

  LoginScreenState() {
    _presenter = new LoginScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  void _submit() {
    final form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      _presenter.doLogin(_email, _password);
    }
  }
  void _register() {
    Navigator.of(_ctx).pushReplacementNamed("/register");

  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
    if (state == AuthState.LOGGED_OUT)
      Navigator.of(_ctx).pushReplacementNamed("/login");
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    var loginBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Sign In"),
      color: Colors.white,
    );
    var registerBtn = new RaisedButton(
      onPressed: _register,
      child: new Text("Create Account"),
      color: Colors.white,
    );
    var fbLogin = new RaisedButton(
      onPressed: _login,
      child: new Text("Continue with Facebook"),
      color: Colors.white,
    );
    var loginForm =
    new Container(
        margin: const EdgeInsets.only(left: 20.0, right: 20.0,top:60.0),
        child: new Column(
          children:
          <Widget>[
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    'assets/img/logo.png', width: 60.0, height: 60.0,
                  ),
                ],
              ),
            ),
            new Padding(
              padding: const EdgeInsets.all(8.0),
              child:  new Text(
                "Welcome to Avanturistic",
                textScaleFactor: 1,
                style: new TextStyle(
                    color: Colors.white),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.all(8.0),
              child:  new Text(
                "Sign In",
                textScaleFactor: 1.5,

              ),
            ),

            new Form(
              key: formKey,
              child: new Column(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      onSaved: (val) => _email = val,
                      validator: (val) {
                        if(val.isEmpty)
                          return 'Email is required.';
                        if(!(EmailValidator.validate(val)))
                          return 'Enter valid email address.';
                        return null;
                      },
                      decoration: new InputDecoration(labelText: "Email"),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TextFormField(
                      obscureText: true,
                      validator: (val) {
                        if(val.isEmpty)
                          return 'Password is required.';
                        return null;
                      },
                      onSaved: (val) => _password = val,
                      decoration: new InputDecoration(labelText: "Password"),
                    ),
                  ),
                ],
              ),
            ),
            _isLoading ? new CircularProgressIndicator() : loginBtn,
            Row(
                children: <Widget>[
                  Expanded(
                      child: Divider()
                  ),

                  Text("or"),

                  Expanded(
                      child: Divider()
                  ),
                ]
            ),
            _isLoading ? fbLogin: fbLogin,
            _isLoading ? registerBtn: registerBtn,

          ],
          crossAxisAlignment: CrossAxisAlignment.center,
            )
        );
    return new WillPopScope(
      onWillPop: _onBackPressed,
      child: new  Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/img/cover.jpg"),
              fit: BoxFit.cover),
        ),
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: new Container(
                child: loginForm,

                decoration: new BoxDecoration(
                    color: Colors.white.withOpacity(0.3)),
              ),
            ),
          ),
        ),
      ),
      ),
    );
    }

  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onLoginSuccess(User user) async {
    //_showSnackBar(user.toString());
    setState(() => _isLoading = false);
    var db = new DatabaseHelper();
    await db.saveUser(user);

    Navigator.of(_ctx).pushReplacementNamed("/home");
  }
}