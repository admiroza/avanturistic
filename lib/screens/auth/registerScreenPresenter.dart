import 'package:avanturistic/data/rest.dart';
import 'package:avanturistic/models/user.dart';

abstract class RegisterScreenContract {
  void onRegisterSuccess(User user);
  void onRegisterError(String errorTxt);
}

class RegisterScreenPresenter {
  RegisterScreenContract _view;
  RestDatasource api = new RestDatasource();
  RegisterScreenPresenter(this._view);

  doRegister(String email, String password, String name) {

    api.register(email, password, name).then((User user) {

      _view.onRegisterSuccess(user);
    }).catchError((Object error) => _view.onRegisterError(error.toString()));

  }
}