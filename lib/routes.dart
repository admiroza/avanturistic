import 'package:flutter/material.dart';
import 'package:avanturistic/screens/home/index.dart';
import 'package:avanturistic/screens/auth/loginScreen.dart';
import 'package:avanturistic/screens/auth/registerScreen.dart';

final routes = {
  '/login':         (BuildContext context) => new LoginScreen(),
  '/home':         (BuildContext context) => new HomeScreen(),
  '/register':         (BuildContext context) => new RegisterScreen(),
  '/' :          (BuildContext context) => new LoginScreen(),
};