import 'package:flutter/material.dart';
import 'package:avanturistic/data/database.dart';
import 'package:avanturistic/models/user.dart';
import 'package:avanturistic/auth.dart';
import 'package:avanturistic/menuPresenter.dart';

class NavDrawer extends StatefulWidget {
@override
State<StatefulWidget> createState() {
  // TODO: implement createState
  return new NavDrawerState();
}
}

class NavDrawerState extends State<NavDrawer>
    implements NavDrawerContract,  AuthStateListener {
  BuildContext _ctx;
  NavDrawerPresenter _presenter;
  User userModel;
  String userName = '';
  Drawer drawer;
  NavDrawerState() {
    _presenter = new NavDrawerPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }
  @override
  onAuthStateChanged(AuthState state) {
    print('auth state changed');
    if (state == AuthState.LOGGED_IN)
      Navigator.of(_ctx).pushReplacementNamed("/home");
    if (state == AuthState.LOGGED_OUT)
      Navigator.of(_ctx).pushReplacementNamed("/login");
  }


  void logout(){
    _presenter.logout();
  }
  loadData() async {


      var db = new DatabaseHelper();
      var  user =  db.getUser();
      setState(() {
        user.then((User user) {
          setState(() =>  userName = user.name );

          print(user.name);
        },
            onError: (e) {
              Navigator.of(_ctx).pushReplacementNamed("/login");
            });
      });


  }

  @override
  void initState() {
    super.initState();
    print(userName);
    if(userName == ''){
      loadData();
    }
  }


  @override
  Widget build(BuildContext context) {
    _ctx = context;

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              userName,
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/img/cover.jpg'))),
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Welcome'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Profile'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Settings'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.border_color),
            title: Text('Feedback'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () {

              logout();
               // Navigator.of(context).pushReplacementNamed("/login");
             // Navigator.of(context).pop();
             // print('logout');

             //  var authStateProvider = new AuthStateProvider();
             //   authStateProvider.notify(AuthState.LOGGED_OUT);
              //  Navigator.of(context).pushReplacementNamed("/login");


            },

          ),
        ],
      ),
    );
  }
  @override
  void onLogout() async {

    var db = new DatabaseHelper();
    await db.deleteUsers();
    Navigator.of(_ctx).pushReplacementNamed("/login");
  }

}