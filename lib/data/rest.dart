import 'dart:async';

import 'package:avanturistic/utils/http.dart';
import 'package:avanturistic/models/user.dart';
import 'package:avanturistic/models/post.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "https://avanturistic.com/api";
  static final LOGIN_URL = BASE_URL + "/login";
  static final REGISTER_URL = BASE_URL + "/register";
  static final GET_POSTS_URL = BASE_URL + "/getPosts";
  static final _API_KEY = "mikrokompjutermikrokontroler";

  Future<User> login(String email, String password) {
    return _netUtil.post(LOGIN_URL, body: {
      "token": _API_KEY,
      "email": email,
      "password": password
    }).then((dynamic res) {
      print(res["error"].toString());
      if (res.containsKey('error'))
        throw new Exception(res["error"].toString());

      return new User.map(res["user"]);
    });
  }

  Future<User> register(String email, String password, String name) {
    print('api register');
    return _netUtil.post(REGISTER_URL, body: {
      "token": _API_KEY,
      "email": email,
      "password": password,
      "name": name
    }).then((dynamic res) {
      if (res == null)
        throw new Exception('Failed to create account. Try again later.');
      if (res.containsKey('error'))
        throw new Exception(res["error"].toString());

      return new User.map(res["user"]);
    });
  }

  Future<List<Post>> getPosts(String email) {
    print('api get posts');
    return _netUtil.post(GET_POSTS_URL, body: {
      "token": _API_KEY,
      "email": email,

    }).then((dynamic res) {
      print(res['posts']);
      if (res == null)
        throw new Exception('Failed to create account. Try again later.');
      if (res.containsKey('error'))
        throw new Exception(res["error"].toString());
      List<Post> postsList = <Post>[];

      for (var i = 0; i < ( res["posts"].length); i++) {

        postsList.add(new Post.map(res["posts"][i]));
      }
      return postsList;

    });
  }
}