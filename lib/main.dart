import 'package:flutter/material.dart';
import 'package:avanturistic/auth.dart';
import 'package:avanturistic/routes.dart';

void main() => runApp(new AvanturisticApp());

class AvanturisticApp extends StatelessWidget {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        color:  Colors.white,
      theme: new ThemeData(
        primarySwatch: Colors.lightGreen,
      ),
      routes: routes,
    );
  }

}